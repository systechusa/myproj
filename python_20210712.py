# -*- coding: utf-8 -*-
"""Python 20210712.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1qGgQL2Dt4Ep33Cq5n8Gpfz5oyumITrOT
"""

print('Hi')

8+1

a = 'Hi'
print(a)

a5 = "Hi"
print(a5)

"""Numbers

String

List

Tuple

Dictionary

Set

Numpy Array

Pandas Series

Pandas DataFrame
"""

a=8.9

b=5

c = a+b

print(c)

str1 = 'Python'

str2 = 'I am an Indian'

str3 = 'I am learning'

str3+' '+str1

""" P Y T H O N

 0 1 2 3 4 5
 
-6-5-4-3-2-1

"""

str1[-1]

str1[0:3]

str1[1:]

str1[-2:]

5

str(5)

num_5 = 5
str_5 = '5'

type(num_5)

type(str_5)

l = [1,2,34,67]

l.append(5)

l

t = (1,2)

shiyaam_details = {'Name':'Shiyaam', 'Place':'Chennai', 'Age':28}

janani_details = {'Name':'Janani', 'Place':'Dindigul'}

shiyaam_details['Age']=29

shiyaam_details

janani_details['Age']=16

janani_details

"""FLOW CONTROL"""

a=15

if a<10:
  print('Value is less than 10')

if a<10:
  print('Value is less than 10')
else:
  print('Value is greater than or equal to 10')

4%2

5%2

b=7

if b%2==0:
  print('Value is divisible by 2')
elif b%3==0:
  print('Value is divisible by 3')
else:
  print('Value is nither divisible by 2 nor 3')

b=14

if b%3==0:
  if b%2==0:
    print('Valid even number')
  else:
    print('Valid odd number')
else:
  print('Invalid Number')

"""for

while
"""

l= [1,2,3,56,99,4,5]

range(100)

range(5,100)

import time
time.sleep(2)

for num in l:
  print(num)
  if num%2==0:
    print('Even number')
  else:
    print('Odd number')
  time.sleep(3)

for num in range(10):
  print(num)

for num in range(2,10):
  print(num)

for num in l:
  print(num)

len(l)

l[4]

for num in range(len(l)):
  print(l[num])

c=1
while c<10:
  print(c)
  c=c+1
  time.sleep(3)

dhoni_scores = [0,25,30,16,12,147,62,183,35,52,17,8,20]

for score in dhoni_scores:
  if score<100:
    print(score)

i = 0
while dhoni_scores[i]<100:
  print(dhoni_scores[i])
  i=i+1

for score in dhoni_scores:
  if score<100:
    print(score)
  else:
    break

"""aabaacccycb - c

hello - l

hiiii - i

haabccdddad - d

arr = [array of numbers]

distance = number

limit = number

[][][]......[]
"""

